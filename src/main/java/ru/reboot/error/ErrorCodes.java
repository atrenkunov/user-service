package ru.reboot.error;

public enum ErrorCodes {
    ILLEGAL_ARGUMENT,
    DUPLICATE_LOGIN,
    DUPLICATE_USERID,
    USER_NOT_FOUND,
    CANT_CREATE_NEW_USER,
    CANT_UPDATE_USER
}
