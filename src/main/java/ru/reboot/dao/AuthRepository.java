package ru.reboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.reboot.dao.entity.UserEntity;

public interface AuthRepository extends JpaRepository<UserEntity, String> {
}
