package ru.reboot.dao.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @Column(name = "userId")
    private String userId;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "secondName")
    private String secondName;

    @Column(name = "birthDate")
    private LocalDate birthDate;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "roles")
    private String roles;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        String stringRoles;
        if (roles != null) {
            stringRoles = String.join(",", roles);
        } else {
            stringRoles = null;
        }
        return "User{" +
                "user_id='" + userId + '\'' +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                ", second_name='" + secondName + '\'' +
                ", birth_date=" + birthDate +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", roles='" + stringRoles + '\'' +
                '}';
    }

    public static class Builder {
        private UserEntity obj;

        public Builder() {
            obj = new UserEntity();
        }

        public Builder setUserID(String userId) {
            obj.userId = userId;
            return this;
        }

        public Builder setFirstName(String firstName) {
            obj.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            obj.lastName = lastName;
            return this;
        }

        public Builder setSecondName(String secondName) {
            obj.secondName = secondName;
            return this;
        }

        public Builder setBirthDate(LocalDate birthDate) {
            obj.birthDate = birthDate;
            return this;
        }

        public Builder setLogin(String login) {
            obj.login = login;
            return this;
        }

        public Builder setPassword(String password) {
            obj.password = password;
            return this;
        }

        public Builder setRoles(List<String> roles) {

            if (Objects.isNull(roles) || roles.isEmpty()) {
                obj.roles = null;
            } else {
                obj.roles = String.join(",", roles);
            }
            return this;
        }

        public UserEntity build() {
            return obj;
        }
    }
}
